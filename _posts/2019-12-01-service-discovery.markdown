---
layout: post
title: "Service Discovery"
date: 2019-12-01
description: "A high level overview of the Goving Trainance services"
image: /assets/images/p-i-high-level-services.png
author: Daniel Raloff
tags: 
    - Advent of Code 2019
---
## A Grand Machine
What sort of services go into a multinational corporation like
Prosperic-Interetty? Those who have worked in a complex enterprise
environment could probably bring to mind quite a few.

There might be:
- a directory service for authentication and authorization
- email servers
- sales CRM
- Business Intelligence platform
- maintenance ticketing system
- project management databases
- content management systems
- file sharing
- public-facing websites

Not to mention the ad-hoc data gathering grounds that tend to clump up
in departments as a matter of need and convenience. Things like common
networked drives, 3rd-party services like Slack or Basecamp, Google
drive, and the like.

Prosperic-Interetty has all of these things and more, each business
unit making decisions ad-hoc or as prescribed by service contracts
negotiated at the corporate level.

Eventually, it will come down to this project to refresh, renew, or
replace many of these services, very few things are not fair game for
rebuilding.

Let's take a look at some of the services defined in just one of the
business units.

## High Level Subset
![A flowchart diagram of a subset of the Prosperic-Interetty Goving Trainance services](/assets/images/p-i-high-level-services-subset.svg)

### Web Services
The above diagram shows a high level overview of the services involved
in just one of P-I's many business units: Prosperic-Interetty Goving
Trainance (PIGT).

At the public facing edge of the graph, we have three services, the
public P-I website, the P-I Goving Trainance Marketplace (PIGTM), and
the home-grown sales CRM. The public website exists for the same
reason most business websites do: to explain what Prosperic-Interetty
does and to entice people to call or e-mail a sales representative.

Both call requests and contact form submissions are sent to the the
Sales CRM as leads, where they are stored until acted upon by the
Goving Trainance Sales Department.

You can see from the diagram that that is not the only interaction the
public site has with other services. The site pulls in content
published from the web content management system (Web CMS), and
displays it as blog posts, advertisements, specials, FAQs, and the
like.

This Web CMS actually services many of the business units'
websites, and each unit will have its own team of editors and
copywriters on staff to generate this content.

### Analytics and Data
Another important interaction is the in-house analytics engine. Many
of the actions and page views taken on the P-I public site trigger
transactions that are delivered to the analytics solution where they
can be evaluated against PIGT goals and metrics as defined by the
business unit leadership and the marketing department.

You'll see also that daily rollups from both the Sales CRM (detailing
points of contact made in either direction as well as other sales
statistics) and the analytics solution are archived and lifted into
the Data Warehouse. While there are multiple Data Warehouse icons in
this graph, they all represent the same data warehouse implementation.

So many systems eventually connect back to the data warehouse, it
becomes more convenient just to duplicate its icon through these
diagrams, analogous to the ground symbol in an electrical diagram.

The business intelligence system has been developed over the last 15 or so years
into a highly complex machine that, while no easy task to master or make changes
to, delivers hundreds of reports each quarter-hour to all the business units in
the company.

Pulling in data from the warehouse, statistical analyses made by the
P-I Data Science team, and hand-submitted reports from processes that
have not yet been integrated into the data warehouse system, P-I
Business Intelligence is the data engine that runs many of the
business decisions in the entire corporation.

One such department that utterly relies upon the business intelligence
platform is the accounting department. Receivables, payables,
depreciation reports, departmental budget breakdowns, petty cash
expenditures, all of these things eventually must flow through the
PIBI platform that produces the reports the accounting department must
have to do its job.

## Highly Nested
Of course, each one of these services is not a monolith. There are
many smaller services and pieces of tech that contribute to the
functioning and reliability of each. HAProxy boxes managing service
pools, message queues to enable asynchronous communication, database,
ETL pipelines, web servers, caches, DNS, firewalls. Any one of the
these services can be broken down into many components, and each of
those components might also be able to be broken down into constituent
parts. 

## Hidden Corners Acknowledgement
Even in a high-level overview it might be possible to have missed
something important in this picture. Anybody who's worked in a mature,
complex computing environment knows that there's always a chance that
there's some box out there doing some important work silently and
invisibly... until somebody accidentally turns it off or its drive
fails.

That's why it's so important when duplicating an under-specified
service to not only capture inbound requests and responses to those
requests, you should also be monitoring the filesystem, looking for
unexpected changes between backups, and capturing outbound traffic of
all kinds.

The last thing you want is to replace a service, have the
green-blue deployment go smoothly, and then discover that once a week
some box rsyncs over a product feed that drives recommended products
or somesuch.

## What's next
Keep on the lookout! Tomorrow we'll be doing a deep dive on the
Prosperic-Interetty public-facing website. We'll be discussing some of
the problems with it and what the PIGT stakeholders are looking to
improve, and how we'll be approaching that.

Look out for screenshots galore!
