---
layout: post
title: "Home Page Design"
date: 2019-12-02
description: "A look into the current and future design decisions for the P-I homepage"
image: /assets/images/office.jpg
author: Daniel Raloff
tags: 
    - Advent of Code 2019
    - Design
---

## So 2008

The last time the Prosperic-Interetty public site got a redesign was 2008, when
skeumorphism and gradients reigned supreme. Mobile website design was not yet
emphasized. Styling was done as much with jQuery as it was with CSS.

![The Prosperic-Interetty home page circa 2008 ... and today](/assets/images/old-p-i-homepage.png)

So, if we're going to be refreshing P-I into a modern, tech-forward company,
we're going to need to help it along with a nice design refresh. Along the
way, we'll also be working on instrumenting the site with new analytics
features.

Keep in mind our diagram from [yesterday](/2019/12/01/service-discovery), that the new
design not only needs to inform visitors of what goving trainance is and how the
PIGT unit can help them, it also needs to guide them towards contacting a
salesperson to set up a beginning consultation.

## Funnels

The way the current site is set up, there's only one call-to-action on the page.
See it? It's the "Contact Us" link at the top of the page. Contact you? Why?
What will you do for me? There's information on this page about what goving
trainance is and why you might need it, but nothing about what to expect a
contact might do.

Research has been done that shows that visitors respond better to more direct
verbs than a simple "Contact Us" or "Get Started". A better CTA for
Prosperic-Interetty might be "Schedule a Consultation" or "Get Trainance!".

<p class="codepen" data-height="194" data-theme-id="default" data-default-tab="result" data-user="KOMON" data-slug-hash="ExaaYjB" style="height: 194px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Prosperic-Interrety: New CTA">
  <span>See the Pen <a href="https://codepen.io/KOMON/pen/ExaaYjB">
  Prosperic-Interrety: New CTA</a> by Daniel Raloff (<a href="https://codepen.io/KOMON">@KOMON</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

Which brings us to another point: styling. Relying on a simple link in the upper
right hand corner is a great way to get your call-to-action ignored. If the
purpose of a page is to get a visitor to take a certain action, you want that
action front and center in your design. Use some contrasting colors to make the
action really pop out. Center it on the page or take advantage of your design
hierarchy to make it a natural part of the flow of the document.

You also want to reiterate that call-to-action throughout the body of the page.
At least every one-and-a-half screenfuls of content would be ideal. If that many
CTAs seems excessive, consider it a challenge to keep your content to the point.

You may also opt to keep the CTA in the header, reducing how many times it needs
to be duplicated throughout the site. If you go this route, keep the header
visible as the user scrolls. Make sure that a CTA is visible and reachable as
often as possible.

If your site has multiple pages, consider how each page might contribute to your
funnel. An "About Us" page might encourage a visitor, "Like what you see? Let's
talk!" with a button saying, "Schedule a consultation" for example.

<p class="codepen" data-height="542" data-theme-id="default" data-default-tab="result" data-user="KOMON" data-slug-hash="XWJJWrd" style="height: 542px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Prosperic-Interrety: About Us CTA">
  <span>See the Pen <a href="https://codepen.io/KOMON/pen/XWJJWrd">
  Prosperic-Interrety: About Us CTA</a> by Daniel Raloff (<a href="https://codepen.io/KOMON">@KOMON</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

On tactic that many product-based businesses, or businesses where their services
fill multiple needs of a broad range of potential customers might use is to have
links to various "Solutions".

A solution in this context is a description of a problem statement typical of
the target audience, as well as a description of how the product or service
might solve that need. If you've done the work to build exemplar customer
profiles, each "Solution" might be thought of a "why" someone would pay you.

A great way "Solution" content might drive your funnel is by catching the
attention of a potential customer with a scenario that indicates to them that
the product was designed with their potential use-case specifically in mind.
Then, intrigued, they might read that solution content and have yet another
chance to be presented with a call to action.

## A New Logo, for starters

There's many more aspects of this redesign that will take more than a day to
analyze and process and be hashed out, and much more thinking and content than
could fit in a single, daily blog post. As well as the PIGT site has been
working these past 11 years, there's been a decline in its utility for
bringing in new customers and leads, and we believe that we can boost that
considerably by making it part of the first efforts of the refresh.

In the meantime, I'd be amiss for giving you nothing in return for reading that
great long section about sales funnels. So here's a sneak peek at a potential
replacement for the logo you've seen in the screenshot:

![Prosperic Interetty new logo candidate](/assets/images/new-logo.png)

# What's Next?
Stay tuned til tomorrow where we'll cover some more concrete elements of the
redesign, and our process for approaching those. Ciao!
